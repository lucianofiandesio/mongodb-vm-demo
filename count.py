#!/usr/bin/env python3

from pymongo import MongoClient
import db_utils

def main():

    resp = db_utils.with_collection().count_documents({})
    print ("collection 'items' doc count:", resp)

if __name__ == "__main__":
    main()