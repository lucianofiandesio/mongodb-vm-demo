#!/usr/bin/env python3
from numpy.random import randint
from numerize import numerize
import random
import numpy as np
import time
from tqdm import tqdm
from pymongo import MongoClient
import db_utils

TABLE_NAME="vm-tag-item"
BILLION = 1000000000
ITEM_SIZE = 300000000
MIN_TAG_PER_ITEM=4
MAX_TAG_PER_ITEM=16
BATCH_SIZE=5000


def main():
    t0 = time.time() 
    item_ids = randint(1, ITEM_SIZE*2, ITEM_SIZE) 
    d = time.time() - t0
    print ("generated " + numerize.numerize(ITEM_SIZE) + " item ids: %.2f s." % d)

    # generated 25K random tag id
    tag_ids = randint(100, 50000, 25000)
    
    collection = db_utils.with_collection()

    buffer = []
    for item_id in tqdm(item_ids):
        #print(len(buffer))
        if len(buffer) >= BATCH_SIZE:
            try:
                batch_write(collection, buffer)
            except Exception as e:
                pass # do nothing
            finally:
                buffer.clear()
                
        # gen random number of tags for item
        number_of_tags = random.randint(MIN_TAG_PER_ITEM, MAX_TAG_PER_ITEM)
    
        # pick random tag ids from array 
        selected_tags = random_sample(tag_ids, number_of_tags)
        an_item = {
            "item_id" : item_id.item(),
            "tag_ids" : selected_tags.tolist()
        }

    
        buffer.append(an_item)
        #print(buffer)
    # write last batch
    batch_write(collection, buffer)
        
    print("done in %.2f s." % (time.time() -t0))

def batch_write(table, items):
    try:
        table.insert_many(items)

    except Exception as e:
        print(e)
    


def random_sample(arr: np.array, size: int = 1) -> np.array:
    return arr[np.random.choice(len(arr), size=size, replace=False)]

if __name__ == "__main__":
    main()
