#!/usr/bin/env python3
from pymongo import MongoClient

import db_utils

def main():

    items = db_utils.with_collection().find( { "tag_ids": { "$all": [ 3066, 5023 ] } } )
    for item in items:
        print(item) 

if __name__ == "__main__":
    main()