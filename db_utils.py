from pymongo import MongoClient

URL="mongodb://localhost:27017/"
USERNAME="mongouser"
PASSWORD="S3cret" 
DB="poc"
COLLECTION="items"



def with_collection():

	client = MongoClient(URL, username=USERNAME, password=PASSWORD)
    
	database = client[DB]   
	collection = database[COLLECTION]

	return collection