numerize==0.12
numpy==1.22.3
tqdm==4.64.0
pymongo==4.1.1
