#!/usr/bin/env python3

from pymongo import MongoClient
import db_utils

def main():

    resp = db_utils.with_collection().create_index([ ("tag_ids", 1) ] , name='TagsIndex')
    print ("index response:", resp)

if __name__ == "__main__":
    main()